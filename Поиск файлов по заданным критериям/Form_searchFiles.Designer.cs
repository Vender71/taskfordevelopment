﻿namespace Поиск_файлов_по_заданным_критериям
{
    partial class Form_searchFiles
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBox_startPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_fileNameTemplate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_textFile = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button_search = new System.Windows.Forms.Button();
            this.label_timerTime = new System.Windows.Forms.Label();
            this.label_nowProcessed = new System.Windows.Forms.Label();
            this.label_fileCount = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.textBox_resultsTree = new System.Windows.Forms.TextBox();
            this.button_startStop = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox_startPath
            // 
            this.textBox_startPath.Location = new System.Drawing.Point(149, 34);
            this.textBox_startPath.Name = "textBox_startPath";
            this.textBox_startPath.Size = new System.Drawing.Size(264, 20);
            this.textBox_startPath.TabIndex = 0;
            this.textBox_startPath.TextChanged += new System.EventHandler(this.textBox_startPath_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Стартовая директория";
            // 
            // textBox_fileNameTemplate
            // 
            this.textBox_fileNameTemplate.Location = new System.Drawing.Point(149, 62);
            this.textBox_fileNameTemplate.Name = "textBox_fileNameTemplate";
            this.textBox_fileNameTemplate.Size = new System.Drawing.Size(264, 20);
            this.textBox_fileNameTemplate.TabIndex = 0;
            this.textBox_fileNameTemplate.TextChanged += new System.EventHandler(this.textBox_fileNameTemplate_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Шаблон имени файла";
            // 
            // textBox_textFile
            // 
            this.textBox_textFile.Location = new System.Drawing.Point(179, 92);
            this.textBox_textFile.Multiline = true;
            this.textBox_textFile.Name = "textBox_textFile";
            this.textBox_textFile.Size = new System.Drawing.Size(234, 89);
            this.textBox_textFile.TabIndex = 0;
            this.textBox_textFile.TextChanged += new System.EventHandler(this.textBox_textFile_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Текст содержащийся в файле";
            // 
            // button_search
            // 
            this.button_search.Location = new System.Drawing.Point(338, 187);
            this.button_search.Name = "button_search";
            this.button_search.Size = new System.Drawing.Size(75, 23);
            this.button_search.TabIndex = 2;
            this.button_search.Text = "Поиск";
            this.button_search.UseVisualStyleBackColor = true;
            this.button_search.Click += new System.EventHandler(this.button_search_Click);
            // 
            // label_timerTime
            // 
            this.label_timerTime.AutoSize = true;
            this.label_timerTime.Location = new System.Drawing.Point(243, 239);
            this.label_timerTime.Name = "label_timerTime";
            this.label_timerTime.Size = new System.Drawing.Size(164, 13);
            this.label_timerTime.TabIndex = 3;
            this.label_timerTime.Text = "Время поиска: Менее секунды";
            // 
            // label_nowProcessed
            // 
            this.label_nowProcessed.AutoSize = true;
            this.label_nowProcessed.Location = new System.Drawing.Point(12, 213);
            this.label_nowProcessed.Name = "label_nowProcessed";
            this.label_nowProcessed.Size = new System.Drawing.Size(207, 13);
            this.label_nowProcessed.TabIndex = 4;
            this.label_nowProcessed.Text = "Файл который сейчас обрабатывается";
            // 
            // label_fileCount
            // 
            this.label_fileCount.AutoSize = true;
            this.label_fileCount.Location = new System.Drawing.Point(12, 239);
            this.label_fileCount.Name = "label_fileCount";
            this.label_fileCount.Size = new System.Drawing.Size(182, 13);
            this.label_fileCount.TabIndex = 5;
            this.label_fileCount.Text = "Количество обработанных файлов";
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // textBox_resultsTree
            // 
            this.textBox_resultsTree.Location = new System.Drawing.Point(419, 34);
            this.textBox_resultsTree.Multiline = true;
            this.textBox_resultsTree.Name = "textBox_resultsTree";
            this.textBox_resultsTree.ReadOnly = true;
            this.textBox_resultsTree.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_resultsTree.Size = new System.Drawing.Size(344, 218);
            this.textBox_resultsTree.TabIndex = 6;
            // 
            // button_startStop
            // 
            this.button_startStop.Enabled = false;
            this.button_startStop.Location = new System.Drawing.Point(246, 187);
            this.button_startStop.Name = "button_startStop";
            this.button_startStop.Size = new System.Drawing.Size(88, 23);
            this.button_startStop.TabIndex = 7;
            this.button_startStop.Text = "Остановить";
            this.button_startStop.UseVisualStyleBackColor = true;
            this.button_startStop.Click += new System.EventHandler(this.button_startStop_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(416, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Найденные файлы";
            // 
            // Form_searchFiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 261);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button_startStop);
            this.Controls.Add(this.textBox_resultsTree);
            this.Controls.Add(this.label_fileCount);
            this.Controls.Add(this.label_nowProcessed);
            this.Controls.Add(this.label_timerTime);
            this.Controls.Add(this.button_search);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_textFile);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_fileNameTemplate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_startPath);
            this.MaximumSize = new System.Drawing.Size(791, 300);
            this.MinimumSize = new System.Drawing.Size(791, 300);
            this.Name = "Form_searchFiles";
            this.Text = "Поиск файлов по заданным критериям";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_searchFiles_FormClosing);
            this.Load += new System.EventHandler(this.Form_searchFiles_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_startPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_fileNameTemplate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_textFile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_search;
        private System.Windows.Forms.Label label_timerTime;
        private System.Windows.Forms.Label label_nowProcessed;
        private System.Windows.Forms.Label label_fileCount;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.TextBox textBox_resultsTree;
        private System.Windows.Forms.Button button_startStop;
        private System.Windows.Forms.Label label4;
    }
}

