﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Поиск_файлов_по_заданным_критериям
{
    public partial class Form_searchFiles : Form
    {
        public Form_searchFiles()
        {
            InitializeComponent();
        }

        DateTime timerTime;
        Thread search;
        string searchingStatus = "not running";

        private void Form_searchFiles_Load(object sender, EventArgs e)
        {            
            textBox_startPath.Text = Properties.Settings.Default.startPath;
            textBox_fileNameTemplate.Text = Properties.Settings.Default.fileNameTemplate;
            textBox_textFile.Text = Properties.Settings.Default.textFile;
            ToolTip toolTipForTextBox = new ToolTip();
            toolTipForTextBox.SetToolTip(textBox_startPath, "Обязательно для заполнения (пример: C:\\Users)");
            toolTipForTextBox.SetToolTip(textBox_fileNameTemplate, "Оставите пустым, для поиска всех файлов (пример: *.txt)");
            toolTipForTextBox.SetToolTip(textBox_textFile, "Оставите пустым, для поиска всех файлов (пример: текст для поиска)");
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            timerTime = timerTime.AddSeconds(1);
            label_timerTime.Text = "Время поиска: " + timerTime.ToString("mm:ss ");
        }

        private void textBox_startPath_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.startPath = textBox_startPath.Text;
            Properties.Settings.Default.Save();
        }

        private void textBox_fileNameTemplate_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.fileNameTemplate = textBox_fileNameTemplate.Text;
            Properties.Settings.Default.Save();
        }

        private void textBox_textFile_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.textFile = textBox_textFile.Text;
            Properties.Settings.Default.Save();
        }

        private void searchFiles() 
        {
            if (textBox_startPath.Text != "")
            {
                searchingStatus = "run";
                Invoke(new Action(() => textBox_resultsTree.Clear()));
                Invoke(new Action(() => label_timerTime.Text = "Время поиска: Менее секунды"));
                timerTime = new DateTime(0, 0);
                Invoke(new Action(() => button_search.Enabled = false));
                Invoke(new Action(() => button_startStop.Enabled = true));
                Invoke(new Action(() => timer.Interval = 1000));
                Invoke(new Action(() => timer.Start()));
                List<string> fileList;
                List<string> directoryForCheckList = new List<string>();
                directoryForCheckList.Add(textBox_startPath.Text);
                string tab = "  ";
                int fileCount = 0;
                string[] tree = textBox_startPath.Text.Split(new char[] { '\\' });
                for (int i = 0; i < tree.Length - 1; i++)
                {
                    Invoke(new Action(() => textBox_resultsTree.AppendText(string.Concat(Enumerable.Repeat(tab, i)) + tree[i] + Environment.NewLine)));
                }
                while (directoryForCheckList.Count > 0)
                {
                    string currentDirectory = directoryForCheckList[0];
                    string[] arrCurrentDirectory = currentDirectory.Split(new char[] { '\\' });
                    try
                    {                        
                        if (textBox_fileNameTemplate.Text != "")
                        {
                            fileList = Directory.GetFiles(currentDirectory, textBox_fileNameTemplate.Text).ToList<string>();
                        }
                        else
                        {
                            fileList = Directory.GetFiles(currentDirectory).ToList<string>();
                        }
                        if (fileList.Count > 0)
                        {
                            List<string> suitableFilesList = new List<string>();                            
                            for (int i = 0; i < fileList.Count; i++)
                            {
                                string[] filePath = fileList[i].Split(new char[] { '\\' });
                                string fileName = filePath.Last();
                                Invoke(new Action(() => label_nowProcessed.Text = "Сейчас обрабатывается: " + fileName));
                                FileInfo file = new FileInfo(fileList[i]);
                                if (file.Length != 0)
                                {
                                    string str = File.ReadAllText(fileList[i]);
                                    if (str.IndexOf(textBox_textFile.Text) != -1)
                                    {
                                        suitableFilesList.Add(string.Concat(Enumerable.Repeat(tab, filePath.Length - 1)) + fileName);
                                    }
                                }
                                else
                                {
                                    if (textBox_textFile.Text == "")
                                    {
                                        suitableFilesList.Add(string.Concat(Enumerable.Repeat(tab, filePath.Length - 1)) + fileName);
                                    }
                                }
                            }
                            if (suitableFilesList.Count > 0)
                            {
                                Invoke(new Action(() => textBox_resultsTree.AppendText(string.Concat(Enumerable.Repeat(tab, arrCurrentDirectory.Length - 1)) + arrCurrentDirectory.Last() + Environment.NewLine)));
                                foreach (string elem in suitableFilesList)
                                {
                                    Invoke(new Action(() => textBox_resultsTree.AppendText(elem + Environment.NewLine)));
                                }
                            }
                        }
                        fileList = Directory.GetFiles(currentDirectory).ToList<string>();
                        fileCount += fileList.Count;
                        Invoke(new Action(() => label_fileCount.Text = "Обработано файлов: " + fileCount));
                        List<string> foundDirectoriesList = Directory.GetDirectories(currentDirectory).ToList<string>();
                        if (foundDirectoriesList.Count > 0)
                        {
                            for (int i = foundDirectoriesList.Count - 1; i != -1; i--)
                            {
                                directoryForCheckList.Insert(0, foundDirectoriesList[i]);
                            }
                        }
                    }
                    catch { }
                    directoryForCheckList.Remove(currentDirectory);
                }
                searchingStatus = "not running";
                Invoke(new Action(() => label_nowProcessed.Text = "Обработано файлов: Обработка файлов завершена"));
                Invoke(new Action(() => timer.Stop()));
                Invoke(new Action(() => button_search.Enabled = true));
                Invoke(new Action(() => button_startStop.Enabled = false));
            }
            else
            {
                MessageBox.Show("Не задана стартовая директория для поиска", "Ошибка", MessageBoxButtons.OK);
            }
        }

        private void button_search_Click(object sender, EventArgs e)
        {
            try
            {
                if (searchingStatus == "not running" || searchingStatus == "stop")
                {
                    search = new Thread(searchFiles);
                    search.Start();
                }
                else
                {
                    if (MessageBox.Show("Есть незавершенный поиск, уверены что хотите начать новый?", "Программа", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        search.Resume();
                        search.Abort();
                        search = new Thread(searchFiles);
                        search.Start();
                        searchingStatus = "run";
                        button_startStop.Text = "Остановить";
                    }
                }
            }
            catch
            {
                MessageBox.Show("Ошибка при нажатии кнопки Поиск", "Ошибка", MessageBoxButtons.OK);
            }
        }

        private void button_startStop_Click(object sender, EventArgs e)
        {
            try
            {
                if (searchingStatus == "run")
                {
                    searchingStatus = "stop";
                    search.Suspend();
                    button_startStop.Text = "Продолжить";
                    button_search.Enabled = true;
                    timer.Stop();
                }
                else
                {
                    searchingStatus = "run";
                    search.Resume();
                    button_startStop.Text = "Остановить";
                    button_search.Enabled = false;
                    timer.Start();
                }
            }
            catch
            {
                MessageBox.Show("Ошибка при нажатии кнопки Остановить\\Продолжить", "Ошибка", MessageBoxButtons.OK);
            }
        }

        private void Form_searchFiles_FormClosing(object sender, FormClosingEventArgs e)
        {            
            if (searchingStatus != "not running")
            {
                if (searchingStatus == "stop")
                {
                    search.Resume();
                }
                search.Abort();
            }            
        }
    }
}
